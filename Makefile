QuickHTML.zip: QuickHTML
	7z a QuickHTML.zip QuickHTML
	rm -rf QuickHTML

QuickHTML: qhtml.exe
	mkdir QuickHTML
	mv qhtml.exe  QuickHTML
	mv plug-ins   QuickHTML
	cp qhtml.nako QuickHTML
	cp LICENSE    QuickHTML
	cp README     QuickHTML
	cp ChangeLog  QuickHTML
	cp Makefile   QuickHTML

qhtml.exe: qhtml.nako
	nakomake qhtml.nako -t cnako -p dnako -p nakoctrl -p nakofile
