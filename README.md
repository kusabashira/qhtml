QuickHTML
====================
QuickHTMLはHTMLのプレビューを見るためのコマンドです、特徴としては

* Chromeのみ対応
* プレビューを起動する度にタブが増えない
* プレビューを起動する度にフォーカスを動かさない

動作環境
====================
Google Chromeの入った
Microsoft Windows 98/Me/2000/XP/Vista/7/8

ライセンス
====================
[NYSL](http://www.kmonos.net/nysl/)

簡単な使い方
====================
cmdの中で

	$ qhtml [HTMLファイルのパス]

するとHTMLのプレビューが表示されます。
(Chromeが起動していなければ起動する)

テキストエディタとの連携例
====================
##Vim
.vimrcに↓を書き加えてください
```vim
let g:isQuitMoment = 0

function! s:exec_QuickHTML()
	if g:isQuitMoment == 1
		g:isQuitMoment = 0
		return
	endif
	!start /b qhtml %:p
endfunction 

function! s:hook_html_writequit_event()
	let g:isQuitMoment = 1
endfunction 

augroup view_html
	autocmd!
	autocmd QuitPre *.html call s:hook_html_writequit_event()
	autocmd Bufwrite *.html call s:exec_QuickHTML()
augroup END
```
これでHTMLファイルを保存したときにプレビューが起動するようになります
(wqで保存したときには起動しません)

##TeraPad
1. メニューバーのツールから、ツールの選択を選択
2. 追加ボタンを押す
3. 名前の欄にQuickHTMLと入力
4. 実行ファイルの欄にqhtml.exeへのフルパスを入力
5. コマンドラインパラメータの欄に%fと入力
6. キーの欄の中でF8を押す(F8が登録済みなら他でも問題無い)
7. そしてOKボタンを押す
これでF8を押すとプレビューが起動するようになります

##OTBEdit
scmlibフォルダの中にotbedit.scmというファイルを作成し
```scm
(app-set-event-handler
	"on-file-saved"
	(lambda ()
		(win-exec "qhtml" (editor-get-filename))))
```
と書いて保存すると、保存時にプレビューが起動するようになります

インストール
====================
QuickHTML.zipを任意のフォルダに展開し、パスを通してください

アンインストール
====================
QuickHTML.zipに入っていたファイルを全て削除して下さい

作者
====================
草柱 kusabashira227@gmail.com
